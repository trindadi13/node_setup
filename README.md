# setup para nodejs com typescript (Babel, Jest, eslint, git/eslint ignore, express)


## Prerequisites
:warning: [Node v14.18.1](https://nodejs.org/en/download/) <br>
:warning: [NPM v6.14.15](https://docs.npmjs.com/cli/v6/commands/npm-install) <br>
:warning:[Extensão Eslint (vscode)](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
```
git clone https://gitlab.com/trindadi13/node_setup.git 
```
```
cd setup_node
```
```
yarn || npm install
```
```
yarn start
```

